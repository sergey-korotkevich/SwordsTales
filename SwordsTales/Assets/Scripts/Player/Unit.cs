﻿
using UnityEngine;

public class Unit : MonoBehaviour
{
    protected virtual void Die()
    {
        
    } 
    
    public virtual void ReceiveDamage(int damage)
    {
    }
}
