﻿using UnityEngine;

namespace UI
{
    public class LevelStart : MonoBehaviour
    {
        [SerializeField] public float transitionTime = 1f;

        public Animator transition;

        private void Awake()
        {
            gameObject.SetActive(true);
        }

    
    }
}
