﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Slider healthBarSlider;

        public void HealthBarValue(float health)
        {
            healthBarSlider.value = health;
        }
    }
}
